<?php

class Post extends AppModel {
	public $name = 'Post';

	public $validate = array(
		'title' => 'notEmpty',
		'category' => 'notEmpty',
		'content' => 'notEmpty',
		);

	public $hasMany = array(
		"Answer" => array(
			'className' => 'Answer',
			'conditions' => '',
			'order' => '',
			'dependetn' => false,
			'foreignKey' => 'post_id'
			)
		);

	public $belongsTo = array(
		"Category" => array(
			'className' => 'Category',
			'conditions' => '',
			'order' => '',
			'dependetn' => false,
			'foreignKey' => 'category_id'
			),
		"User" => array(
			'className' => 'User',
			'conditions' => '',
			'order' => '',
			'dependetn' => false,
			'foreignKey' => 'user_id'
			)
		);

}

?>