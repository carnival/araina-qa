<?php
class Answer extends AppModel {
	public $name = 'Answer';

	public $validate = array(
		'content' => 'notEmpty'
		);
	
	public $belongsTo = array(
		"User" => array(
			'className' => 'User',
			'conditions' => '',
			'order' => '',
			'dependetn' => false,
			'foreignKey' => 'user_id'
			)
		);


}

?>