<?php 

class PostController extends AppController{
	public $name = 'Post';
	public $uses = array('Post', 'Answer', 'Category', 'User');
	// public $autoLayout = false;
	public $layout = 'Bootstrap';

	public function index(){
		$this->layout="top";
		$data = null;
		if(!empty($this->data)){
			$data = $this->Post->find('all');
		}else{
			$data = $this->Post->find('all', array('order' => array('Post.id' => 'desc')));
		}

		$this->set('data',$data);

	}

	public function search(){
		$data = null;
		if(!empty($this->data)){
			$content = $this->data['Post']['content'];
			$conditions = array(
                    'OR' => array(
                        array('Post.content like' => "%{$content}%"),
                        array('Post.title like' => "%{$content}%")
                    )
            );
			$data = $this->Post->find('all', array('conditions' => $conditions));

		}else{
			$data = $this->Post->find('all');
		}

		$this->set('data',$data);
	}		

	public function addRecord(){
		if(!empty($this->data)){
			$flag = $this->Post->save($this->data);
			if($flag){
				$this->set("result", $flag);
				// $this->redirect('www.google.com');
			}
		}

	}

	public function detail(){
		//ID等を取得
		if (isset($this->params['pass'][0])) {
			//URLパスに、Postidが入っている場合
			//IDから情報を取得
			$postid = $this->params['pass'][0];
			$post_content = $this->Post->find('all', array('conditions' => array('Post.id' => $postid)));
			$this->set('post_content',$post_content);
			
			//回答のユーザーを取得
			$answer = $this->Answer->find('all', 
				array(
					'conditions' => array('Answer.post_id' => $postid),
					'order' => array('Answer.created' => 'DESC')
					));
			
			$this->set('answer', $answer);

			// $answer = $post_content['Answer'];
			// $this->set('answer', $answer);

		}else{
			echo "getパラメータに何も入ってないよ";
		} 
	}

	public function addAnswer(){
		if(!empty($this->data)){
			$flag =	$this->Answer->save($this->data);
			if($flag){
				$this->set("result", $flag);	
			}
		}
	}

}


?>