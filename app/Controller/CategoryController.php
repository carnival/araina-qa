<?php

class CategoryController extends AppController {
	
	public $name = 'Category';
	public $uses = array('Category', 'Post');
	public $layout = 'Bootstrap';


	public function index(){
		$data = $this->Category->find('all');
		$this->set('data', $data);
	}

	public function view(){
		if (isset($this->params['pass'][0])) {
			$category_id = $this->params['pass'][0];
			$post_content = $this->Post->find('all', array('conditions' => array('Post.category_id' => $category_id)));
			$this->set('post_content',$post_content);

		}else{
			echo "getパラメータに何も入ってないよ";
		} 
	}


}
