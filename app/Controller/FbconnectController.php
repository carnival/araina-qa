<?php
 
//【1】facebook認証
// App::import('Vendor','facebook',array('file' => 'facebook'.DS.'src'.DS.'facebook.php'));
App::import('Vendor','facebook/facebook'); 
 
class FbconnectController extends AppController {
    public $name = 'Fbconnect';
 
    public function beforeFilter()
    {
        parent::beforeFilter();
         $this->facebook = new Facebook(array(
         'appId' => '1419905464954434',
         'secret' => '65d5015321dc0c41485d1dda81a950ed',
         'cookie' => true,
         ));
        
    }

    function index(){
        $this->autoRender = false;//echo $callback = Configure::read('callback');die('test1');
        $url = $this->facebook->getLoginUrl(
            array('redirect_url' => '','scope' => 'user_interests,email,user_likes,user_birthday,user_interests,user_about_me,user_activities','canvas' => 1,'fbconnect' => 0));
        $this->redirect($url);
    }
 
    function showdata(){//トップページ
        $facebook = $this->createFacebook(); //【2】アプリに接続
        $myFbData = $this->Session->read('mydata');       //【3】facebookのデータ
        $this->set('myFbData', $myFbData);
    }
 
    //facebookの認証処理部分
    public function facebook(){
        $this->autoRender = false;
        $this->facebook = $this->createFacebook();
        $user = $this->facebook->getUser();       //【4】ユーザ情報取得
        if($user){//認証後
            $me = $this->facebook->api('/me','GET',array('locale'=>'ja_JP'));  //【5】ユーザ情報を日本語で取得
            $this->Session->write('mydata',$me);      //【6】ユーザ情報をセッションに保存
            $this->redirect('showdata');
        }else{//認証前
            $url = $this->facebook->getLoginUrl(array(
            'scope' => 'email,publish_stream,user_birthday','canvas' => 1,'fbconnect' => 0));   //【7】スコープの確認
            $this->redirect($url);
        }
    }
 
    private function createFacebook() {        //【8】appID, secretを記述
        return new Facebook(array(
            'appId' => '1419905464954434',
            'secret' => '65d5015321dc0c41485d1dda81a950ed'
        ));
    }

    public function logout(){
        $facebook = new Facebook(array(
          'appId'  => Configure::read("facebook.APP_ID"),
          'secret' => Configure::read("facebook.APP_SECRET"),
        ));
        $params = array( 'next' => 'http://cake.hakomori.net/');
        $logoutUrl = $facebook->getLogoutUrl($params);
        $facebook->destroySession();
    }
}