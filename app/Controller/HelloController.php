<?php

class HelloController extends AppController {
	
	public $name = 'Hello'; //Controllerの名前
	public $uses = null; //使用するModel

	public function index (){
		App:uses('Sanitize', 'Utility');
		$result = "";
		if($this->request->isPost()){
			$result = "<pre>送信された情報<br />";
			foreach ($this->request->data['HelloForm'] as $key => $val) {
				$result .= $key . ' => ' . $val;
			}
		$result .= "</pre>";
	} else {
		$result = "何か書いて下さい";
	}
	
	$this->set("result", Sanitize::stripScripts($result));
}

}