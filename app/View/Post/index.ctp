
<h3>最新の質問</h3>
<hr>
<?php
for($i = 0; $i < 10; $i++){
	$arr = $data[$i]['Post'];
	echo "<a href='post/detail/". $arr['id'] ."'>";
	echo "<div class='post_box'>";
	echo "<ul><li>";
	echo "{$arr['created']}   　　　　投稿者:<span class='gender_{$data[$i]['User']['gender']}'>" . $data[$i]['User']['nickname'] ."</span>   　　　　回答数:". count($data[$i]['Answer'])."<br />";
	echo "<span class='post_cat_{$arr['category_id']} post_cat'>".$data[$i]['Category']['name']."</span>";
	echo "<span class='post_title'>{$arr['title']}</span><br />";
	echo "<div class='post_content'>{$arr['content']}</div>";	
	echo "</li></ul>";
	echo "</div>";
	echo "</a><br />";
 }
?>


<h3>質問する</h3>

<?php 
echo $this->Form->create(false, array('type'=>'post', 'action'=>'addRecord', 'class'=>'form-horizontal'));
// echo $this->Form->text('Post.user_id');
echo $this->Form->text('Post.title',array('class'=>'form-control','placeholder' => 'タイトル'));
echo $this->Form->radio('Post.category_id',
	array('1'=>'エンタメ','2'=>'グルメ', '3'=>'住まい','4'=>'仕事', '5'=>'旅行', '6'=>'手続き','7'=>'デモ','0'=>'その他'),array('class'=>'form-radio', 'legend' => false));
echo $this->Form->textarea('Post.content',array('class'=>'form-control','placeholder' => '質問本文'));
echo "<br />";
$msg = __('登録しますか？', true);
echo $this->Form->submit(__('質問する', true), array('onClick'=>"return confirm('$msg')", 'class'=>'btn btn-default'));
// echo $this->Form->submit('質問する', array('class'=>'btn btn-default'));
echo $this->Form->end();
?>
