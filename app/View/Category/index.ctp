<?php
	echo "<h2>カテゴリー 一覧</h2>";

	echo "<ul>";
	for ($i=0; $i < count($data); $i++) { 
		echo "<a href='/category/view/" . $data[$i]['Category']['id'] . "'>";
		echo "<li class='category_list'>";
		echo $data[$i]['Category']['name'];
		echo "(" . count($data[$i]['Post']) . ")";
		echo "</li>";
		echo "</a>";
	}
	echo "</ul>";

	// echo "<pre>";
	// print_r($data);
	// echo "</pre>";
?>